// Initialize an instance because it is a class
const EventEmitter = require('events')
const my = new EventEmitter()
const covid = require ('../../week 2/day_9/assignment-day-9-Q2.js')

readline = require('readline')
rl = readline.createInterface({
    input : process.stdin,
    output : process.stdout
})

function loginFailed(email) {
    // TO DO: Saving the login trial count in the database
    console.log(email + ' Gagal masuk! ');
    rl.close()
}

// Registering a listener
my.on('Login Failed', loginFailed)

function loginSucess(email) {
    console.log(email + ' Login Berhasil!\n ');
    covid.menu()
}

my.on('Login Success', loginSucess)

// const user = {
    function login(email, password) {
      const pass = 123456;
  
      if (password != pass) {
        my.emit('Login Failed', email); // Pass the email to the listener
      } else {
        // Do something
        my.emit('Login Success', email)
      }
    }
  // }
  
  rl.question("Email: ", function(email) {
    rl.question("Password: ", function(password) {
      login(email, password) // Run login function
    })
  })

  module.exports.rl = rl
