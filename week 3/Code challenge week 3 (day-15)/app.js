const express = require ('express') 
const app = express()
const amandaRoutes = require('./routers/amandaRoutes.js')

app.use(express.static('public'));

app.use('/', amandaRoutes)

app.listen(8000)