const express = require('express')
const router = express.Router()
const AmandaController = require('../controllers/amandaController.js')

router.get('/', AmandaController.home)
router.get('/index', AmandaController.home)

module.exports = router;