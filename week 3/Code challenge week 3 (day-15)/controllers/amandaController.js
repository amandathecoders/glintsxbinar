class AmandaController {

    async home(req, res) {
        try {
            console.log("You're accessing localhost of amanda!");

            res.render('amanda.ejs')
        
        } catch (e) {
            res.status(500).send(e)
        }
    }

    
}

module.exports = new AmandaController;