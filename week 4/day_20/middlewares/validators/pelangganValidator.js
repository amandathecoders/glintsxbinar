const { Pelanggan } = require("../../models") // Import models
const { check, validationResult, matchedData, sanitize } = require('express-validator');

const multer = require ('muter');
const path = require('path');
const crypto = require('crypto');


module.exports = {
    create : [
      check('nama').isString(),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],
    
      update: [
        //Set form validation rule
        check('nama').isString(),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],


};