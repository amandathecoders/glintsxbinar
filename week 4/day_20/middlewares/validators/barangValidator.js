const { Barang, Pemasok } = require("../../models") // Import models
const { check, validationResult, matchedData, sanitize } = require('express-validator');

const multer = require ('multer');
const path = require('path');
const crypto = require('crypto');// to encrypt something

/* Used to upload image */
const uploadDir = '/img/';  // make images upload to /img/
const storage = multer.diskStorage({
    destination: "../../public" + uploadDir, // make images upload to /public/img/
    filename: function(req, file, cb) {
      crypto.pseudoRandomBytes(16, function(err, raw) {
        if (err) return cb(err)
  
        cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
      })
    }
  })

const upload = multer({ storage: storage, dest: uploadDir }); // if we want to upload an image we can use it!
/* End used to upload image */


module.exports = {
  create : [
    upload.single('image'),

    check('id_pemasok').isNumeric().custom(value => {
      return Pemasok.findOne({
        where: {
          id: value
        }
      }).then(p => {
        if (!p) {
          throw new Error('ID pemasok tidak ada!');
        }
      })
    }),
    check('harga').isNumeric(),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      },
    ],

    update: [
        //Set form validation rule
        check('id_pemasok').isNumeric().custom(value => {
          return Pemasok.findOne({
            where: {
              id: value
            }
          }).then(b => {
            if (!b) {
              throw new Error('ID pemasok tidak ada!');
            }
          })
        }),
        check('harga').isNumeric(),
        check('id').custom(value => {
          return Barang.findOne ({
            where : {
              id: value
            }
          }).then (p => {
            if (!p) {
              throw new Error('ID barang tidak ada!')
            }
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],

      delete: [
        //Set form validation rule
        check('id_barang').isNumeric().custom(value => {
          return barang.findOne({
            where: {
              isbn: value
            }
          }).then(b => {
            if (!b) {
              throw new Error('Id_barang not found');
            }
          })
        }),
      ]
    
    };
          

