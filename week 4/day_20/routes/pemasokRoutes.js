const express = require('express') // Import express
const router = express.Router() // Make router from app
const PemasokController = require('../controllers/barangController.js') // Import TransaksiController
const pemasokValidator = require('../middlewares/validators/barangValidator.js') // Import validator to validate every request from user

router.get('/', PemasokController.getAll) 
router.get('/:id', PemasokController.getOne) 
router.post('/create', pemasokValidator.create, PemasokController.create) 
router.put('/update/:id', pemasokValidator.update, PemasokController.update) 
router.delete('/delete/:id', PemasokController.delete) 

module.exports = router; // Export router