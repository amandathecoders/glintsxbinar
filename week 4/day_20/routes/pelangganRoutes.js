const express = require('express') // Import express
const router = express.Router() // Make router from app
const PelangganController = require('../controllers/barangController.js') // Import TransaksiController
const pelanggankValidator = require('../middlewares/validators/barangValidator.js') // Import validator to validate every request from user

router.get('/', PelangganController.getAll) 
router.get('/:id', PelangganController.getOne) 
router.post('/create', pelanggankValidator.create, PelangganController.create) 
router.put('/update/:id', pelanggankValidator.update, PelangganController.update) 
router.delete('/delete/:id', PelangganController.delete) 

module.exports = router; // Export router