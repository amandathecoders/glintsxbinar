const express = require('express') // Import express
const router = express.Router() // Make router from app
const BarangController = require('../controllers/barangController.js') // Import TransaksiController
const barangValidator = require('../middlewares/validators/barangValidator.js') // Import validator to validate every request from user

router.get('/', BarangController.getAll) 
router.get('/:id', BarangController.getOne) 
router.post('/create', barangValidator.create, BarangController.create) 
router.put('/update/:id', barangValidator.update, BarangController.update) 
router.delete('/delete/:id', BarangController.delete) 

module.exports = router; // Export router
