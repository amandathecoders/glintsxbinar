const { Transaksi, Barang, Pelanggan } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator');

class PelangganController {
    
    // Get All data from pelanggan
async getAll(req, res) {
    Pelanggan.findAll({ // find all data of Transaksi table
      attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed

    }).then(pelanggan => {
      res.json(pelanggan) // Send response JSON and get all of Transaksi table
    })
  }

// Get One data from transaksi
async getOne(req, res) {
    Pelanggan.findOne({ // find one data of Transaksi table
      where: {
        id: req.params.id // where id of Transaksi table is equal to req.params.id
      },
      attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed

    }).then(pelanggan => {
      res.json(pelanggan) // Send response JSON and get one of Transaksi table depend on req.params.id
    })
  }

 // Create Transaksi data
 async create(req, res) {
    Pelanggan.create({  // find one data of Barang table
        nama: req.body.nama
    }).then(newpelanggan => {
      // Send response JSON and get one of Transaksi table that we've created
      res.json({
        "status": "success",
        "message": "new pelanggan added",
        "data": newpelanggan
      })
    })
  }

// Update Transaksi data
async update(req, res) {
    Pelanggan.update({  // find one data of Barang table
      where: {
        id: req.body.id // where id of Barang table is equal to req.params.id
      },
    }).then(Pelanggan => {
        var update = {
            nama: req.body.nama
        }

      // Transaksi table update data
      return Pelanggan.update(update, {
        where: {
          id: req.params.id
        }
      })
    }).then(affectedRow => {
      return Pelanggan.findOne({ // find one data of Transaksi table
        where: {
          id: req.params.id  // where id of Transaksi table is equal to req.params.id
        }
      })
    }).then(p => {
      // Send response JSON and get one of Transaksi table that we've updated
      res.json({
        "status": "success",
        "message": "pelanggan updated",
        "data": p
      })
    })
  }

// Soft delete Barang data
async delete(req, res) {
    Pelanggan.destroy({  // Delete data from Barang table
      where: {
        id: req.params.id  // Where id of Barang table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "transaksi deleted",
          "data": null
        }
      }

      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }



}

module.exports = new PelangganController;