const { Barang, Pemasok } = require ('../models');
const { check, validationResult, matchedData, sanitize } = require ('express-validator');

class BarangController {

    constructor () {
        Pemasok.hasMany(Barang, {
            foreignKey : 'id_pemasok'
        }),
        Barang.belongsTo(Pemasok, {
            foreignKey : 'id_pemasok'
        })
    }

    // Get All data from barang
 async getAll(req, res) {
    Barang.findAll({ // find all data of Barang table
      attributes: ['id', 'nama', 'harga', ['createdAt', 'waktu']], // just these attributes that showed
      include: [{
        model: Pemasok,
        attributes: ['nama'] // just this attrubute from Barang that showed
      }]
    }).then(barang => {
      res.json(barang) // Send response JSON and get all of Barang table
    })
  }

  // Get One data from barang
  async getOne(req, res) {
    Barang.findOne({ // find one data of Barang table
      where: {
        id: req.params.id // where id of Barang table is equal to req.params.id
      },
      attributes: ['id', 'barang', 'harga', ['createdAt', 'waktu']], // just these attributes that showed
      include: [{
        model: Pemasok,
        attributes: ['nama'] // just this attrubute from Barang that showed
      }]
    }).then(barang => {
      res.json(barang) // Send response JSON and get one of Barang table depend on req.params.id
    })
  }

// Create Barang data
async create(req, res) {
    // Pemasok.findOne({  // find one data of Pemasok table
    //   where: {
    //     id: req.body.id_pemasok // where id of Pemasok table is equal to req.params.id
    //   },
    //   attributes: ['nama'] // Get nama from Pemasok
    // }).then(nama => {
    //     res.json(nama) // Send response JSON and get one of Pemasok table depend on req.params.id
    
    //   // Barang table create data
    //   return 
        Barang.create({
        nama: req.body.id_nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
        image: req.file === undefined ? "" : req.file.filename
      })
    // })
      .then(newBarang => {
      // Send response JSON and get one of Barang table that we've created
      res.json({
        "status": "success",
        "message": "transaksi added",
        "data": newBarang
      })
    })
  }

  // Update Barang data
  async update(req, res) {

      // Make update query
      var update = {
        nama: req.body.id_nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
      }

      // Barang table update data
      Barang.update(update, {
        where: {
          id: req.params.id
        }
      }).then(affectedRow => {
      return Barang.findOne({ // find one data of Barang table
        where: {
          id: req.params.id  // where id of Barang table is equal to req.params.id
        }
      })
    }).then(p => {
      // Send response JSON and get one of Barang table that we've updated
      res.json({
        "status": "success",
        "message": "transaksi updated",
        "data": p
      })
    })
  }

// Soft delete Barang data
async delete(req, res) {
    Barang.destroy({  // Delete data from Barang table
      where: {
        id: req.params.id  // Where id of Barang table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "transaksi deleted",
          "data": null
        }
      }

      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }

}

module.exports = new BarangController;