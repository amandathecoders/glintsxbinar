const { Pemasok } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator');
const pelangganController = require("./pelangganController");

class PemasokController {

    async getAll(req, res) {
        Pemasok.findAll({ 
          attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed
    
        }).then(pemasok => {
          res.json(pemasok) 
        })
      }

      async getOne(req, res) {
        Pemasok.findOne({ // find one data of Transaksi table
          where: {
            id: req.params.id // where id of Transaksi table is equal to req.params.id
          },
          attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed
    
        }).then(pemasok => {
          res.json(pemasok) // Send response JSON and get one of Transaksi table depend on req.params.id
        })
      }

    async create(req, res) {
        Pemaosk.create({  // find one data of Barang table
            nama: req.body.nama
        }).then(newPemasok => {
          // Send response JSON and get one of Transaksi table that we've created
          res.json({
            "status": "success",
            "message": "new pelanggan added",
            "data": newPemasok
          })
        })
      }

    async update(req, res) {
        Pemasok.update({  // find one data of Barang table
          where: {
            id: req.body.id // where id of Barang table is equal to req.params.id
          },
        }).then(Pemasok => {
            var update = {
                nama: req.body.nama
            }
    
          // Transaksi table update data
          return Pemasok.update(update, {
            where: {
              id: req.params.id
            }
          })
        }).then(affectedRow => {
          return Pemasok.findOne({ // find one data of Transaksi table
            where: {
              id: req.params.id  // where id of Transaksi table is equal to req.params.id
            }
          })
        }).then(p => {
          // Send response JSON and get one of Transaksi table that we've updated
          res.json({
            "status": "success",
            "message": "pelanggan updated",
            "data": p
          })
        })
      }
    
      async delete(req, res) {
        Pemasok.destroy({  // Delete data from Barang table
          where: {
            id: req.params.id  // Where id of Barang table is equal to req.params.id
          }
        }).then(affectedRow => {
          // If delete success, it will return this JSON
          if (affectedRow) {
            return {
              "status": "success",
              "message": "transaksi deleted",
              "data": null
            }
          }
    
          // If failed, it will return this JSON
          return {
            "status": "error",
            "message": "Failed",
            "data": null
          }
        }).then(r => {
          res.json(r) // Send response JSON depends on failed or success
        })
      }
        
    
}
module.exports = new PemasokController;