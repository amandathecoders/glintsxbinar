// //from index.js


// const data = require('./arrayFactory.js');
// const test = require('./test.js');

// /*
//  * Code Here!
//  * */

// // Optional
// function clean(data) {
//   return data.filter(i => typeof i === 'number');
// }

// // Should return array
// function sortAscending(data) {
//   // Code Here
//     data.sort(clean)
//   return data;
// }

// // Should return array
// function sortDecending(data) {
//   // Code Here
//     data.reverse()
//   return data;
// }

// // DON'T CHANGE
// test(sortAscending, sortDecending, data);






const data = require('./arrayFactory.js');
const test = require('./test.js');

function clean(data) {
  return data.filter(i => typeof i === 'number');
}

function sortAscending(data) {
  var data = clean(data);
  for (let j = 0; j < data.length; j++) {
    for (let k = 0; k < data.length; k++) {
      if (data[k] > data[k + 1]) {
        let tmp = data[k];
        data[k] = data[k + 1];
        data[k + 1] = tmp;
      }
    }
  }
  return data;
};

function sortDescending(data) {
  var data = clean(data);
  for (let j = 0; j < data.length; j++) {
    for (let k = 0; k < data.length; k++) {
      if (data[k] < data[k + 1]) {
        let tmp = data[k];
        data[k] = data[k + 1];
        data[k + 1] = tmp;
      }
    }
  }
  return data;
};

test(sortAscending, sortDescending, data);

module.exports = data