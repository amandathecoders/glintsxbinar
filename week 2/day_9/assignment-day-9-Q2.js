// Question 2

const login = require('../../week 3/day_11/login.js')

// const readline = require('readline')
// const rl = readline.createInterface({
//     input : process.stdin,
//     ouput : process.stdout
// })

let person = [{
    name : 'John',
    status: 'positive'
},  
{
    name : 'Mike',
    status : 'suspect'
},
{
    name : 'Sony',
    status : 'negative'
}];

function positive () {
    for ( var j = 0; j < person.length; j++){
        if (person[j].status.includes('positive')){
            console.log(person[j].name)
        }
    }
}

function suspect () {
    for ( var j = 0; j < person.length; j++){
        if (person[j].status.includes('suspect')){
            console.log(person[j].name)
        }
    }
}

function negative () {
    for ( var j = 0; j < person.length; j++){
        if (person[j].status.includes('negative')){
            console.log(person[j].name)
        }
    }
}

function menu() {
    console.log('Menu');
    console.log('======');
    console.log('1. Positive');
    console.log('2. Suspect');
    console.log('3. Negative');
    console.log('4. Exit');
    login.rl.question('Choose option :', option => {
        switch (Number(option)) {
            case 1 :
                positive();
                menu();
                break;

            case 2:
                suspect();
                menu();
                break;

            case 3:
                negative();
                menu();
                break;
            
            case 4:
                login.rl.close();
                break;

            default :
                console.log('Please choose number 1 or 2 or 3 only!\n');
                menu()
        }
    })
}

// menu()

module.exports.menu = menu


// function positive() {
//     for (var i = 0; i < person.length; i++){
//         if (person[i].length)) {
//             console.log(person.name)
            
//         }
//     }
// }



