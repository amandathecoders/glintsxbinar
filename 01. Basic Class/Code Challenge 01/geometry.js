// Cube volume calculation
function calculateCubeVolume (s) {
    return s**3;
}
console.log(calculateCubeVolume (5));

//Beam volume Calculation
function calculateBeamVolume (w, l, h) {
    var surfaceArea = w * l;
    return surfaceArea * h;
}
console.log(calculateBeamVolume(3, 10, 2.5));

//Triangular Prism Volume Calculation
function calculatePrismVolume (l, w, h) {
    const baseArea = 0.5 * l * w;
    return baseArea * h;
}
console.log(calculatePrismVolume(6, 3, 8));
