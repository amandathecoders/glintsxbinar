const { pemasok, barang} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

const multer = require ('multer');
const path = require('path');
const crypto = require('crypto');// to encrypt something

/* Used to upload image */
const uploadDir = '/img/';  // make images upload to /img/
const storage = multer.diskStorage({
    destination: "./public" + uploadDir, // make images upload to /public/img/
    filename: function(req, file, cb) {
      crypto.pseudoRandomBytes(16, function(err, raw) {
        if (err) return cb(err)
  
        cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
      })
    }
  })

const upload = multer({ storage: storage, dest: uploadDir }); // if we want to upload an image we can use it!
/* End used to upload image */

module.exports = {
    getOne: [
      
        check('id').custom(value => {
          return barang.findOne({
            _id: value
          }).then(result => {
            if (!result) {
              throw new Error('ID barang tidak ada!')
            }
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        }
      ],
    
    create: [
        //Set form validation rule
        upload.single('image'),
        check('nama').isString(),
        check('harga').isNumeric(),
        check('id_pemasok').custom(value => {
          return pemasok.findById(value).then(b => {
            if (!b) {
              throw new Error('ID pemasok tidak ada!');
            }
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],
    
      update: [
        //Set form validation rule
        upload.single('image'),
        check('id').custom(value => {
          return barang.findById(value).then(b => {
            if (!b) {
              throw new Error('ID barang tidak ada!');
            }
          })
        }),
        check('nama').isString(),
        check('harga').isNumeric(),
        check('id_pemasok').custom(value => {
          return pemasok.findById(value).then(p => {
            if (!p) {
              throw new Error('ID pemasok tidak ada!');
            }
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],

      delete: [
        check('id').custom(value => {
          return barang.findOne({
            _id: value
          }).then(result => {
            if (!result) {
              throw new Error('ID barang tidak ada!')
            }
          }).catch(error => {
            throw new Error('ID barang tidak ketemu!')
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        }
      ]
}