const { pemasok } = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

module.exports = {

    getOne: [
        check('id').custom(value => {
          return pemasok.findOne({
            _id: value
          }).then(result => {
            if (!result) {
              throw new Error('ID pemasok tidak ada!')
            }
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        }
      ],

      create: [
        //Set form validation rule
        check('nama').isString(),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],

      update: [
        //Set form validation rule
        check('id').custom(value => {
          return pemasok.findById(value).then(b => {
            if (!b) {
              throw new Error('ID pemasok tidak ada!');
            }
          })
        }),
    
        check('nama').isString(),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        },
      ],

      delete: [
        check('id').custom(value => {
          return pemasok.findOne({
            _id: value
          }).then(result => {
            if (!result) {
              throw new Error('ID pemasok tidak ada!')
            }
          }).catch(error => {
            throw new Error('ID pemasok tidak ketemu!')
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        }
      ]
}