const express = require('express') // import express
const router = express.Router() // import router
const transaksiValidator = require('../middlewares/validators/transaksiValidator') //import transaksi validator
const TransaksiController = require('../controllers/transaksiController') // import transaksi controller

router.get('/', TransaksiController.getAll)
router.get('/:id', transaksiValidator.getOne, TransaksiController.getOne)
router.post('/create', transaksiValidator.create, TransaksiController.create)
router.put('/update/:id', transaksiValidator.update, TransaksiController.update) 
router.delete('/delete/:id', transaksiValidator.delete, TransaksiController.delete)

module.exports = router; // export router