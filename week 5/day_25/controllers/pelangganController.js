const { pelanggan } = require('../models')

class PelangganController {

    //get all data
    async getAll(req, res) {
        //find all data
        pelanggan.find({}).then(result => {
            res.json({
                status: 'success',
                data: result 
            })
        })
    }

    //get one data
    async getOne(req, res) {
        // find one data
        pelanggan.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
              status: "success",
              data: result
            })
        })
    }
    // create
    async create(req, res) {
    
        pelanggan.create({
          "nama": req.body.nama,
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
    }
    //update
    async update(req, res) {
  
        pelanggan.findOneAndUpdate({
          _id: req.params.id
        }, {
            "nama": req.body.nama

        }).then(() => {
          return pelanggan.findOne({
            _id: req.params.id
          })
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
    }
    //delete
    async delete(req, res) {
        pelanggan.delete({
          _id: req.params.id
        }).then(() => {
          res.json({
            status: "success",
            data: null
          })
        })
      }

}
module.exports = new PelangganController;
