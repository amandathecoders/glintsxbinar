const { pemasok } = require('../models')

class pemasokController {

    //get all data
    async getAll(req, res) {
        //find all data
        pemasok.find({}).then(result => {
            res.json({
                status: 'success',
                data: result 
            })
        })
    }

    //get one data
    async getOne(req, res) {
        // find one data
        pemasok.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
              status: "success",
              data: result
            })
        })
    }
    // create
    async create(req, res) {
    
        pemasok.create({
          "nama": req.body.nama,
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
    }
    //update
    async update(req, res) {
  
        pemasok.findOneAndUpdate({
          _id: req.params.id
        }, {
            "nama": req.body.nama

        }).then(() => {
          return pemasok.findOne({
            _id: req.params.id
          })
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
    }
    //delete
    async delete(req, res) {
        pemasok.delete({
          _id: req.params.id
        }).then(() => {
          res.json({
            status: "success",
            data: null
          })
        })
      }

}
module.exports = new pemasokController;
