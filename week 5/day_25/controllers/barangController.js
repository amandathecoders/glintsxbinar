const { pemasok, barang } = require('../models')

class BarangController {

    //get all data
    async getAll(req, res) {
        //find all data
        barang.find({}).then(result => {
            res.json({
                status: 'success',
                data: result 
            })
        })
    }

    //get one data
    async getOne(req, res) {
        // find one data
        barang.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
              status: "success",
              data: result
            })
        })
    }
    // create
    async create(req, res) {
        const data = await Promise.all([
          pemasok.findOne({
            _id: req.body.id_pemasok
          }),
        ])
        barang.create({
          image:req.file===undefined?"":req.file.filename,
          "nama": req.body.nama,
          "harga": eval(req.body.harga),
          "pemasok": data[0]
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
    }
    //update
    async update(req, res) {
        const data = await Promise.all([
          pemasok.findOne({
            _id: req.body.id_pemasok
          }),
        ])
  
        barang.findOneAndUpdate({
          _id: req.params.id
        }, {
          image:req.file===undefined?"":req.file.filename ,
            "nama": req.body.nama,
            "harga": eval(req.body.harga),
            "pemasok": data[0]
        }).then(() => {
          return barang.findOne({
            _id: req.params.id
          })
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
    }
    //delete
    async delete(req, res) {
        barang.delete({
          _id: req.params.id
        }).then(() => {
          res.json({
            status: "success",
            data: null
          })
        })
      }

}
module.exports = new BarangController;