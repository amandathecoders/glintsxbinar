const mongoose = require('mongoose') //import mongoose
const mongoose_delete = require('mongoose-delete') // import mongoose-delete to make soft delete

// make transaksi schema
const TransaksiSchema = new mongoose.Schema({
    //define column
    barang: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    pelanggan: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    jumlah: {
        type: Number,
        required: true
    },
    total: {
        type: mongoose.Schema.Types.Decimal128,
        required: true
    }
}, {
    //Enable timestamps
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'update_at'
    },
    versionKey: false // disable __v column

});

TransaksiSchema.plugin(mongoose_delete, {overrideMethods: 'all'}) //enable softdelete

module.exports = transaksi = mongoose.model('transaksi', TransaksiSchema, 'transaksi'); // export transaksi model