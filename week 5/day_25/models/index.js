const mongoose = require('mongoose') //Import mongoose

const uri = 'mongodb://localhost:27017/penjualan_dev' // Url database mongodb

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true }) // make connection hto mongodb penjualan_dev database

const barang = require('./barang') // import model barang
const pelanggan = require('./pelanggan') // import model pelanggan
const pemasok = require('./pemasok') // import model pemasok
const transaksi = require('./transaksi') // import model transaksi

module.exports = { barang, pelanggan, pemasok, transaksi }; // exports models
