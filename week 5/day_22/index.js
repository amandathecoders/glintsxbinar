const express = require('express') // Import express
const app = express() // Make express app
const bodyParser = require('body-parser') 
const transaksiRoutes = require('./routes/transaksiRoutes.js') // Import bodyParser to read post body 

//Set body parser for HTTP post operation
app.use(bodyParser.json()); //suport json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies

app.use('/transaksi', transaksiRoutes) //if accessing localhost:3000/transaksi/, it will go to transaksiRoutes variable

app.listen(3000) //make port 3000 for this app