const { ObjectId } = require('mongodb')
const client = require('../models/connection.js')

class PemasokController {

    //get all data
    async getAll(req,res) {
        const penjualan = client.db('penjualan') //Connect to penjualan database
        const pemasok = penjualan.collection('pemasok') // connect to transaksi collection / table

        //find all pelanggan data
        pemasok.find({}).toArray().then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }
    
    // get one data
    async getONe(req, res) {
        const penjualan = client.db('penjualan') //Connect to penjualan
        const pemasok = penjualan.collection('pemasok') //connect to transaksi collection / table


        //Find one data
        pemasok.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status:'success',
                data: resut
            })
        })
    }

    // create a data
    async create(req, res) {
        const penjualan = client.db('penjualan') // Connect to penjualan database
        const pemasok = penjualan.collection('pemasok') // Connect to transaksi collection / table

        // Insert data pelanggan
        pemasok.insertOne({
        nama: req.body.nama
        }).then(result => {
        res.json({
            status: "success",
            data: result.ops[0]
        })
        })
    }

    // update a data pelanggan
    async update(req, res) {
        const penjualan = client.db('penjualan') // Connect to penjualan database
        const pemasok = penjualan.collection('pemasok') // Connect to transaksi collection / table


        // Update data depends on pelanggan
        pemasok.updateOne({
        _id: new ObjectId(req.params.id)
        }, {
        $set: {
            nama: req.body.nama
        }
        }).then(() => {
        return pemasok.findOne({
            _id: new ObjectId(req.params.id)
        })
        }).then(result => {
        res.json({
            status: 'success',
            data: result
        })
        })
    }

    // delete data
    async delete(req, res) {
        const penjualan = client.db('penjualan') // Connect to penjualan database
        const pemasok = penjualan.collection('pemasok') // Connect to transaksi collection / table

        // delete data depends on req.params.id
        pemasok.deleteOne({
        _id: new ObjectId(req.params.id)
        }).then(result => {
        res.json({
            status: 'success',
            data: null
        })
        })
    }

}
module.exports = new PemasokController;