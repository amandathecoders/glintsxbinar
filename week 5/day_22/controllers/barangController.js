const { ObjectId } = require('mongodb')
const client = require('../models/connection.js')

class BarangController {

    async getAll(req,res) {
        const penjualan = client.db('penjualan') //Connect to penjualan database
        const barang = penjualan.collection('barang') // connect to transaksi collection / table

        //find all transaksi data
        barang.find({}).toArray().then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getONe(req, res) {
        const penjualan = client.db('penjualan') //Connect to penjualan
        const barang = penjualan.collection('barang') //connect to transaksi collection / table


        //Find one data
        barang.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status:'success',
                data: resut
            })
        })
    }

    async create(req, res) {
        const penjualan = client.db('penjualan') // Connect to penjualan database
        const barang = penjualan.collection('barang') // Connect to transaksi collection / table

        // Get data pemasok depends on req.body.id_pemasok
        const pemasok = await penjualan.collection('pemasok').findOne({
        _id: new ObjectId(req.body.id_pemasok)
        })

        // Insert data barang
        barang.insertOne({
        nama: req.body.nama,
        harga: req.body.harga,
        pemasok: pemasok
        }).then(result => {
        res.json({
            status: "success",
            data: result.ops[0]
        })
        })
    }

    async update(req, res) {
        const penjualan = client.db('penjualan') // Connect to penjualan database
        const barang = penjualan.collection('barang') // Connect to transaksi collection / table

        
        const pemasok = await penjualan.collection('pemasok').findOne({
        _id: new ObjectId(req.body.id_pemasok)
        })

        // Update data depends on transaksi
        barang.updateOne({
        _id: new ObjectId(req.params.id)
        }, {
        $set: {
            nama: req.body.nama,
            harga: req.body.harga,
            pemasok: pemasok
        }
        }).then(() => {
        return barang.findOne({
            _id: new ObjectId(req.params.id)
        })
        }).then(result => {
        res.json({
            status: 'success',
            data: result
        })
        })
    }

    async delete(req, res) {
        const penjualan = client.db('penjualan') // Connect to penjualan database
        const barang = penjualan.collection('barang') // Connect to transaksi collection / table

        // delete data depends on req.params.id
        barang.deleteOne({
        _id: new ObjectId(req.params.id)
        }).then(result => {
        res.json({
            status: 'success',
            data: null
        })
        })
    }


}
module.exports = new BarangController;