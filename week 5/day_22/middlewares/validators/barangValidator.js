const { check, validationResult} = require('express-validator')
const client = require('../../models/connection.js')
const { ObjectId } = require('mongodb')

module.exports = {
    create: [

      check('nama').isString(),
      (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
      },

      check('harga').isNumber(),
      (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
      },
      
        check('id_pemasok').custom(value => {
          return client.db('penjualan').collection('pemasok').findOne({
            _id: new ObjectId(value)
          }).then(result => {
            if (!result) {
              throw new Error('ID Pemasok tidak ada')
            }
            next()
          })
        }),
      ],

    update: [
      check('id').custom(value => {
          return client.db('penjualan').collection('barang').findOne({
            _id: new ObjectId(value)
          }).then(result => {
            if (!result) {
              throw new Error('ID Barang tidak ada')
            }
          })
        }),
        
      check('nama').isString(),
      (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
      },

      check('harga').isNumber(),
      (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
      },

      check('id_pemasok').custom(value => {
          return client.db('penjualan').collection('pemasok').findOne({
            _id: new ObjectId(value)
          }).then(result => {
            if (!result) {
              throw new Error('ID Pemasok tidak ada')
            }
            next()
          }) 
        }),
      ]
}