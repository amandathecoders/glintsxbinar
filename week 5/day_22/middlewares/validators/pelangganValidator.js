const { check, validationResult} = require('express-validator')
const client = require('../../models/connection.js')
const { ObjectId } = require('mongodb')

module.exports = {

    create: [
        check('nama').isString(),
        (req, res, next) => {
          const errors = validationResult(req)
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next()
        }
      ],
    

      update: [
        check('id').custom(value => {
          return client.db('penjualan').collection('pelanggan').findOne({
            _id: new ObjectId(value)
          }).then(result => {
            if (!result) {
              throw new Error('ID Pelanggan tidak ada')
            }
          })
        }),
        check('nama').isString(),
        (req, res, next) => {
          const errors = validationResult(req)
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next()
        }
      ]

}